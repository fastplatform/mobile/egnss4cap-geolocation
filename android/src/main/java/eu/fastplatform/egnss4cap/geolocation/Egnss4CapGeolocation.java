package eu.fastplatform.egnss4cap.geolocation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.PluginRequestCodes;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;

import eu.foxcom.convex_hull.Cluster;
import eu.foxcom.convex_hull.Point;
import eu.foxcom.gnss_scan.CentroidFilter;
import eu.foxcom.gnss_scan.NMEAScanner;
import eu.foxcom.gnss_scan.NMEAParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class FaSTNMEAParser extends NMEAParser {
  // latest nmea message timestamp
  private Long latestMessageTimestamp;

  public FaSTNMEAParser (Context context) {
    super(context);
  }

  @Override
  public void receive(NMEAScanner.NMEAHolder nmeaHolder) {
    super.receive(nmeaHolder);
    this.setLatestMessageTimestamp(nmeaHolder.getTimestamp());
  }

  public void setLatestMessageTimestamp(Long latestMessageTimestamp) {
    this.latestMessageTimestamp = latestMessageTimestamp;
  }

  public Long getLatestMessageTimestamp() {
    return latestMessageTimestamp;
  }
}

class FaSTCluster extends Cluster {
  private List<Point> fastPoints = new ArrayList();
  private List<NMEAParser.GGAData> fastListGGAData = new ArrayList<>();

  @Override
  public void addPoint(Point point) {
    super.addPoint(point);
    this.fastPoints.add(point);
  }

  @Override
  public void removePoint(Point point) {
    super.removePoint(point);
    this.fastPoints.remove(point);
  }

  public void removeFirstPointAndGGAData() {
    // remove first ggaData
    this.fastListGGAData.remove(0);
    // remove first point
    Point point = this.fastPoints.get(0);
    this.removePoint(point);
  }

  public void addGGAData (NMEAParser.GGAData ggaData) {
    this.fastListGGAData.add(ggaData);
    this.addPoint(new Point(ggaData.getLatitude(), ggaData.getLongitude()));
  }

  public List<Point> getFastPoints() {
    return fastPoints;
  }

  public List<Point> getPoints() {
    return this.getFastPoints();
  }

  public List<NMEAParser.GGAData> getFastListGGAData() {
    return fastListGGAData;
  }
}

@NativePlugin(
    permissions={
      Manifest.permission.ACCESS_COARSE_LOCATION,
      Manifest.permission.ACCESS_FINE_LOCATION
    },
    permissionRequestCode = PluginRequestCodes.GEOLOCATION_REQUEST_PERMISSIONS
)
public class Egnss4CapGeolocation extends Plugin {

  private Map<String, PluginCall> watchingCalls = new HashMap<>();
  private FusedLocationProviderClient fusedLocationClient;
  private LocationCallback locationCallback;
  // EGNSS variables
  private NMEAScanner fastNmeaScanner;
  private FaSTNMEAParser fastNmeaParser; // cf FaSTNMEAParser class above
  private JSObject fastLocationJSObject;
  private Integer fastNmeaSamplingNumber = 7;
  private FaSTCluster fastCluster;

  @PluginMethod()
  public void getCurrentPosition(PluginCall call) {
    if (!hasRequiredPermissions()) {
      saveCall(call);
      pluginRequestAllPermissions();
    } else {
      sendLocation(call);
    }
  }

  private void sendLocation(PluginCall call) {
    requestLocationUpdates(call);
  }

  @PluginMethod(returnType=PluginMethod.RETURN_CALLBACK)
  public void watchPosition(PluginCall call) {
    call.save();
    if (!hasRequiredPermissions()) {
      saveCall(call);
      pluginRequestAllPermissions();
    } else {
      startWatch(call);
    }
  }

  @SuppressWarnings("MissingPermission")
  private void startWatch(PluginCall call) {
    requestLocationUpdates(call);
    watchingCalls.put(call.getCallbackId(), call);
  }

  @SuppressWarnings("MissingPermission")
  @PluginMethod()
  public void clearWatch(PluginCall call) {
    String callbackId = call.getString("id");
    if (callbackId != null) {
      PluginCall removed = watchingCalls.remove(callbackId);
      if (removed != null) {
        removed.release(bridge);
      }
    }
    if (watchingCalls.size() == 0) {
      clearLocationUpdates();
    }
    call.success();
  }

  /**
   * Process a new location item and send it to any listening calls
   * @param location
   */
  private void processLocation(Location location) {
    for (Map.Entry<String, PluginCall> watch : watchingCalls.entrySet()) {
      watch.getValue().success(getJSObjectForLocation(location));
    }
  }

  @Override
  protected void handleRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    super.handleRequestPermissionsResult(requestCode, permissions, grantResults);

    PluginCall savedCall = getSavedCall();
    if (savedCall == null) {
      return;
    }

    for(int result : grantResults) {
      if (result == PackageManager.PERMISSION_DENIED) {
        savedCall.error("User denied location permission");
        return;
      }
    }

    if (savedCall.getMethodName().equals("getCurrentPosition")) {
      sendLocation(savedCall);
    } else if (savedCall.getMethodName().equals("watchPosition")) {
      startWatch(savedCall);
    } else {
      savedCall.resolve();
      savedCall.release(bridge);
    }
  }

  private JSObject getJSObjectForLocation(Location location) {
    JSObject ret = new JSObject();
    JSObject coords = new JSObject();
    ret.put("coords", coords);
    ret.put("timestamp", location.getTime());
    coords.put("latitude", location.getLatitude());
    coords.put("longitude", location.getLongitude());
    coords.put("accuracy", location.getAccuracy());
    coords.put("altitude", location.getAltitude());
    if (Build.VERSION.SDK_INT >= 26) {
      coords.put("altitudeAccuracy", location.getVerticalAccuracyMeters());
    }
    coords.put("speed", location.getSpeed());
    coords.put("heading", location.getBearing());
    return ret;
  }

  private void requestLocationUpdates(final PluginCall call) {
    // If android version allows it, start the GNSS scan along with the gms services
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      _requestLocationUpdatesGNSS();
    }
    _requestLocationUpdatesAndroidGMS(call);
  }

  @SuppressWarnings("MissingPermission")
  private void _requestLocationUpdatesAndroidGMS(final PluginCall call) {
    _clearLocationUpdatesAndroidGMS();
    boolean enableHighAccuracy = call.getBoolean("enableHighAccuracy", false);
    int timeout = call.getInt("timeout", 10000);
    fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

    LocationManager lm = (LocationManager)getContext().getSystemService(Context.LOCATION_SERVICE);
    boolean networkEnabled = false;
    try {
      networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    } catch(Exception ex) {}
    LocationRequest locationRequest = new LocationRequest();
    locationRequest.setMaxWaitTime(timeout);
    locationRequest.setInterval(10000);
    locationRequest.setFastestInterval(5000);
    int lowPriority = networkEnabled ? LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY : LocationRequest.PRIORITY_LOW_POWER;
    int priority = enableHighAccuracy ? LocationRequest.PRIORITY_HIGH_ACCURACY : lowPriority;
    locationRequest.setPriority(priority);

    locationCallback = new LocationCallback(){
      @Override
      public void onLocationResult(LocationResult locationResult) {
        if (call.getMethodName().equals("getCurrentPosition")) {
          clearLocationUpdates();
        }
        Location lastLocation = locationResult.getLastLocation();
        if (lastLocation == null) {
          call.error("location unavailable");
        } else {
          JSObject locationObject = getJSObjectForLocation(lastLocation);
          // *** GNSS ***
          // Add the GNSS stabilized position to the GMS data if available
          if (fastLocationJSObject != null) {
            locationObject.put("latestGnssScanCoords", fastLocationJSObject);
          }
          // return location
          call.success(locationObject);
        }
      }
      @Override
      public void onLocationAvailability(LocationAvailability availability) {
        if (!availability.isLocationAvailable()) {
          call.error("location unavailable");
          clearLocationUpdates();
        }
      }
    };

    fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
  }

  private void clearLocationUpdates() {
    // Clear the ENGSS data and scanner if available
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      _clearLocationUpdatesGNSS();
    }
    // Clear the GMS data
    _clearLocationUpdatesAndroidGMS();

  }

  private void _clearLocationUpdatesAndroidGMS() {
    if (locationCallback != null) {
      fusedLocationClient.removeLocationUpdates(locationCallback);
      locationCallback = null;
    }
  }

  // ****************************************************************
  // ****************** EGNSS Specific functions ********************
  // ****************************************************************

  @RequiresApi(api = Build.VERSION_CODES.N)
  @SuppressWarnings("MissingPermission")
  private void _requestLocationUpdatesGNSS() {
    // reset existing scanner and geolocation data
    _clearLocationUpdatesGNSS();
    // init gnss location object
    fastLocationJSObject = new JSObject();
    // create nmea scanner and parser
    fastNmeaScanner = new NMEAScanner(getContext());
    fastNmeaParser = new FaSTNMEAParser(getContext());
    // create convex hull cluster
    fastCluster = new FaSTCluster();
    // set minimum number of points to compute the centroid
    fastNmeaParser.setSamplingNumber(fastNmeaSamplingNumber);
    // define options for centroid filtering.
    // TODO get those options from the plugin parameters (call object)
    final Integer minNumberSats = 3;
    final Integer minHDOP = 1;
    final Integer minSNR = 10;
    final Integer minFix = 3;
    // define a centroid filter
    final CentroidFilter centroidFilter = new CentroidFilter() {
      @Override
      public boolean testSample(NMEAParser.GGAData ggaData) {
        Integer currMeanSnr = fastNmeaParser.getSNRSatellites().getMeanSnr();
        return (
          ggaData.getSatteliteNumber() != null && ggaData.getSatteliteNumber() >= minNumberSats
            && ggaData.getHdop() != null && ggaData.getHdop() >= minHDOP
            && currMeanSnr != null && currMeanSnr >= minSNR
            && fastNmeaParser.getLastFixValue() != null && fastNmeaParser.getLastFixValue() >= minFix
        );
      }
    };
    fastNmeaParser.setCentroidFilter(centroidFilter);
    // set gga receiver
    fastNmeaParser.setGgaReceiver(new NMEAParser.GGAReceiver() {
      @Override
      public void receive(NMEAParser.GGAData ggaData) {
        if (ggaData.getLatitude() == null || ggaData.getLongitude() == null) {
          System.out.println("Non valid ggaData (no lat. or long.):" + ggaData.toString());
          return;
        }
        if (centroidFilter.testSample(ggaData)) {
          // add gga data to the cluster
          fastCluster.addGGAData(ggaData);
          //fastCluster.addPoint(new Point(ggaData.getLatitude(), ggaData.getLongitude()));
        }
        int processedSampleNumber = fastCluster.getSize();
        if (processedSampleNumber >= fastNmeaParser.getSamplingNumber()) {
          System.out.println("Calculate position");
          List<NMEAParser.GGAData> listGGAData = fastCluster.getFastListGGAData();
          List<Point> lastPerimeter = new ArrayList<>();
          Point centroid = fastCluster.computeCentroid(lastPerimeter);
          // add the calculated position to the location object
          fastLocationJSObject.put("latitude", centroid.getX());
          fastLocationJSObject.put("longitude", centroid.getY());
          Double d = calculateDistance(centroid, lastPerimeter);
          fastLocationJSObject.put("precision", d);
          // set the timestamp of the latest nmea message (technically this one)
          fastLocationJSObject.put("timestamp", fastNmeaParser.getLatestMessageTimestamp());
          // add the list of gga data (points) used to calculate the position
          JSONArray listGGADataAsJSObject = new JSONArray();
          for (int i = 0; i < listGGAData.size(); i++) {
            listGGADataAsJSObject.put(ggaDataToJSObject(listGGAData.get(i)));
          }
          fastLocationJSObject.put("clusterPoints", listGGADataAsJSObject);
          System.out.println(fastLocationJSObject.toString());
          try {
            System.out.println(fastLocationJSObject.get("clusterPoints"));
          } catch (JSONException e) {
            e.printStackTrace();
          }
          // The calculated location is sent when the GMS scanner receive a position => _requestLocationUpdatesAndroidGMS
          // remove oldest point in cluster
          fastCluster.removeFirstPointAndGGAData();
        }
      }
    });
    // register nmea parser
    fastNmeaScanner.registerReceiver(fastNmeaParser);
    // scan till the 'clearWatch' method is called
    fastNmeaScanner.startScan();
  }

  @RequiresApi(api = Build.VERSION_CODES.N)
  private void _clearLocationUpdatesGNSS() {
    if (fastNmeaScanner != null) {
      fastNmeaScanner.stopScan();
    }
    fastNmeaParser = null;
    fastNmeaScanner = null;
    fastLocationJSObject = null;
  }

  private Double calculateDistance(Point point, List<Point> lastPerimeter) {
    Double precision = null;
    Double distance;
    for (int i = 0; i < lastPerimeter.size(); i++) {
      distance = Math.sqrt(Math.pow(point.getX()-lastPerimeter.get(i).getX(), 2) + Math.pow(point.getY()-lastPerimeter.get(i).getY(), 2));
      distance = distance * 60 * 1852;
      if (precision != null) {
        precision = Math.max(precision, distance);
      } else {
        precision = distance;
      }
    }
    return precision;
  }

  private JSObject ggaDataToJSObject (NMEAParser.GGAData ggaData) {
    JSObject jsObject = new JSObject();
    jsObject.put("latitude", ggaData.getLatitude());
    jsObject.put("longitude", ggaData.getLongitude());
    jsObject.put("ew", ggaData.getEW());
    jsObject.put("ns", ggaData.getNS());
    jsObject.put("altitude", ggaData.getAltitude());
    jsObject.put("fixType", ggaData.getFixType());
    jsObject.put("hdop", ggaData.getHdop());
    jsObject.put("satelliteNumber", ggaData.getSatteliteNumber());
    return jsObject;
  }
}