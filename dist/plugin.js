var egnss4capGeolocationPlugin = (function (exports, core) {
    'use strict';

    // credits to the ionic-team:
    // https://github.com/ionic-team/capacitor/blob/2.x/core/src/util.ts
    const extend = (target, ...objs) => {
        objs.forEach(o => {
            if (o && typeof o === 'object') {
                for (var k in o) {
                    if (o.hasOwnProperty(k)) {
                        target[k] = o[k];
                    }
                }
            }
        });
        return target;
    };

    class Egnss4CapGeolocationPluginWeb extends core.WebPlugin {
        constructor() {
            super({ name: 'Egnss4CapGeolocation', platforms: ['web'] });
        }
        getCurrentPosition(options) {
            return new Promise((resolve, reject) => {
                return this.requestPermissions().then((_result) => {
                    window.navigator.geolocation.getCurrentPosition((pos) => {
                        resolve(pos);
                    }, (err) => {
                        reject(err);
                    }, extend({
                        enableHighAccuracy: true,
                        timeout: 10000,
                        maximumAge: 0
                    }, options));
                });
            });
        }
        watchPosition(options, callback) {
            let id = window.navigator.geolocation.watchPosition((pos) => {
                callback(pos);
            }, (err) => {
                callback(null, err);
            }, extend({
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0
            }, options));
            return `${id}`;
        }
        clearWatch(options) {
            window.navigator.geolocation.clearWatch(parseInt(options.id, 10));
            return Promise.resolve();
        }
    }
    const Egnss4CapGeolocation = new Egnss4CapGeolocationPluginWeb();
    core.registerWebPlugin(Egnss4CapGeolocation);

    exports.Egnss4CapGeolocation = Egnss4CapGeolocation;
    exports.Egnss4CapGeolocationPluginWeb = Egnss4CapGeolocationPluginWeb;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

}({}, capacitorExports));
//# sourceMappingURL=plugin.js.map
