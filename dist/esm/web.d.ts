import { WebPlugin } from '@capacitor/core';
import { Egnss4CapGeolocationPlugin, GeolocationOptions, GeolocationPosition, GeolocationWatchCallback } from './definitions';
export declare class Egnss4CapGeolocationPluginWeb extends WebPlugin implements Egnss4CapGeolocationPlugin {
    constructor();
    getCurrentPosition(options?: GeolocationOptions): Promise<GeolocationPosition>;
    watchPosition(options: GeolocationOptions, callback: GeolocationWatchCallback): string;
    clearWatch(options: {
        id: string;
    }): Promise<void>;
}
declare const Egnss4CapGeolocation: Egnss4CapGeolocationPluginWeb;
export { Egnss4CapGeolocation };
