// credits to the ionic-team:
// https://github.com/ionic-team/capacitor/blob/2.x/core/src/util.ts
export const extend = (target, ...objs) => {
    objs.forEach(o => {
        if (o && typeof o === 'object') {
            for (var k in o) {
                if (o.hasOwnProperty(k)) {
                    target[k] = o[k];
                }
            }
        }
    });
    return target;
};
//# sourceMappingURL=util.js.map