import { WebPlugin, registerWebPlugin } from '@capacitor/core';

import {
  PermissionsRequestResult,
  Egnss4CapGeolocationPlugin,
  GeolocationOptions,
  GeolocationPosition,
  GeolocationWatchCallback,
} from './definitions';

import { extend } from './util';

export class Egnss4CapGeolocationPluginWeb extends WebPlugin implements Egnss4CapGeolocationPlugin {
  constructor() {
    super({ name: 'Egnss4CapGeolocation', platforms: ['web'] });
  }

  getCurrentPosition(options?: GeolocationOptions): Promise<GeolocationPosition> {
    return new Promise((resolve, reject) => {
      return this.requestPermissions().then((_result: PermissionsRequestResult) => {
        window.navigator.geolocation.getCurrentPosition((pos) => {
          resolve(pos);
        }, (err) => {
          reject(err);
        }, extend({
          enableHighAccuracy: true,
          timeout: 10000,
          maximumAge: 0
        }, options));
      });
    });
  }

  watchPosition(options: GeolocationOptions, callback: GeolocationWatchCallback): string {
    let id = window.navigator.geolocation.watchPosition((pos) => {
      callback(pos);
    }, (err) => {
      callback(null, err);
    }, extend({
      enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 0
    }, options));

    return `${id}`;
  }

  clearWatch(options: { id: string }) {
    window.navigator.geolocation.clearWatch(parseInt(options.id, 10));
    return Promise.resolve();
  }
}

const Egnss4CapGeolocation = new Egnss4CapGeolocationPluginWeb();

export { Egnss4CapGeolocation };
registerWebPlugin(Egnss4CapGeolocation);