# EGNSS4CAP geolocation capacitor plugin

The project is a capacitor plugin based on the [Geolocation plugin from the capacitor core project (v2.x)](https://github.com/ionic-team/capacitor/tree/2.x) that uses the EGNSS4CAP library to calculate the geolocation based on satellite data.

Currently, the calculation of the geolocation by the EGNSS4CAP library only works on Android. For Web and iOS platforms, this plugin works as the capacitor Geolocation plugin.

> This plugin is not used anymore. It needs to be upgraded to Capacitor 3 before any usage in the current FaST farmer application. 
